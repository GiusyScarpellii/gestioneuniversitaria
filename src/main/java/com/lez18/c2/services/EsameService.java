package com.lez18.c2.services;

import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lez18.c2.models.Esame;
import com.lez18.c2.models.Studente;

@Service
public class EsameService {
	
	@Autowired
	private EntityManager entMan;
	
	protected Session getSession() {
		return entMan.unwrap(Session.class);
	}
	
	public Esame insert(Esame objEs) {
		
		Esame temp = new Esame();
		temp.setNome_esame(objEs.getNome_esame());
		temp.setCodice_esame(objEs.getCodice_esame());
		temp.setCrediti_esame(objEs.getCrediti_esame());
		temp.setData_esame(objEs.getData_esame());
		
		Session session = getSession();
		
		try {
			session.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
		
	}
	
	public Esame findById(int varId) {
		Session session = getSession();
		return(Esame) session
							.createCriteria(Esame.class)
							.add(Restrictions.eqOrIsNull("id", varId))
							.uniqueResult();
	}
	
	public List<Esame> findAll() {
		Session session = getSession();
		return session.createCriteria(Esame.class).list();
	}

	@Transactional
	public boolean delete (int varId) {
		Session session = getSession();
		try {
			Esame temp = session.load(Esame.class, varId);
			
			session.delete(temp);
			session.flush();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	@Transactional
	public boolean update(Esame objEs) {
		Session session = getSession();
		
		try {
			Esame temp = session.load(Esame.class, objEs.getId());
			if(temp != null) {
				temp.setNome_esame(objEs.getNome_esame());
				temp.setCodice_esame(objEs.getCodice_esame());
				temp.setCrediti_esame(objEs.getCrediti_esame());
				temp.setData_esame(objEs.getData_esame());
				
				session.update(temp);
				session.save(temp);
				session.persist(temp);
				session.flush();
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	public List<Esame> findByName (String varNome) {
		Session session = getSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Esame> cq = cb.createQuery(Esame.class);
		Root<Esame> esame = cq.from(Esame.class);
		List<Predicate> predicati = new ArrayList<>();
		if(varNome != null) {
			predicati.add(cb.like(esame.get("nome"), "%" + varNome + "%"));
		}
		cq.where(predicati.toArray(new Predicate[0]));
		return session.createQuery(cq).getResultList();
	}
	
	public List<Esame> findByDate (String varData_esame) {
		Session session = getSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Esame> cq = cb.createQuery(Esame.class);
		Root<Esame> esame = cq.from(Esame.class);
		List<Predicate> predicati = new ArrayList<>();
		if(varData_esame != null) {
			predicati.add(cb.like(esame.get("nome"), "%" + varData_esame + "%"));
		}
		cq.where(predicati.toArray(new Predicate[0]));
		return session.createQuery(cq).getResultList();
	}
	
}
