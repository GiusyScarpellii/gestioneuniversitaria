package com.lez18.c2.services;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lez18.c2.models.Studente;


@Service
public class StudenteService {

	@Autowired
	private EntityManager entMan;
	
	private Session getSession() {
		return entMan.unwrap(Session.class);
	}
	
	public Studente insert(Studente objStud) {
		
		Studente temp = new Studente();
		temp.setNome(objStud.getNome());
		temp.setCognome(objStud.getCognome());
		temp.setMatricola(objStud.getMatricola());
		temp.setNascita(objStud.getNascita());
		
		Session sessione = getSession();
		
		try {
			sessione.save(temp);
			
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return null;
		
	}
	
	public Studente findById(int varId) {
		Session sessione = getSession();
		
		
		return (Studente) sessione
							.createCriteria(Studente.class)
							.add(Restrictions.eqOrIsNull("id", varId))
							.uniqueResult();
	}
	
	public List<Studente> findAll(){
		Session sessione = getSession();
		return sessione.createCriteria(Studente.class).list();
	}
	@Transactional
	public boolean delete (int varId) {
		
		Session session = getSession();
		try {
			
			Studente temp = session.load(Studente.class, varId);
			
			session.delete(temp);
			session.flush();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	@Transactional
	public boolean update(Studente objStud) {
		Session session = getSession();
		
		try {
			Studente temp = session.load(Studente.class, objStud.getId());
			if(temp != null) {
				temp.setNome(objStud.getNome());
				temp.setCognome(objStud.getCognome());
				temp.setMatricola(objStud.getMatricola());
				temp.setNascita(objStud.getNascita());
				
				
				session.update(temp);
				session.save(temp);
				session.persist(temp);
				session.flush();
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	public List<Studente> findByName (String varNome) {
		Session sessione = getSession();
		CriteriaBuilder cb = sessione.getCriteriaBuilder();
		CriteriaQuery<Studente> cq = cb.createQuery(Studente.class);
		Root<Studente> studente = cq.from(Studente.class);
		List<Predicate> predicati = new ArrayList<>();
		if(varNome != null) {
			predicati.add(cb.like(studente.get("nome"), "%" + varNome + "%"));
		}
		cq.where(predicati.toArray(new Predicate[0]));
		return sessione.createQuery(cq).getResultList();
		
	}
	
	
	public List<Studente> findByNameAndSurname (String varNome, String varCognome) {
		Session sessione = getSession();
		CriteriaBuilder cb = sessione.getCriteriaBuilder();
		CriteriaQuery<Studente> cq = cb.createQuery(Studente.class);
		Root<Studente> studente = cq.from(Studente.class);
		List<Predicate> predicati = new ArrayList<>();
		if(varNome != null) {
			predicati.add(cb.like(studente.get("nome"), "%" + varNome + "%"));
		}
		if(varCognome != null) {
			predicati.add(cb.like(studente.get("cognome"), "%" + varCognome + "%"));
		}
		cq.where(predicati.toArray(new Predicate[0]));
		return sessione.createQuery(cq).getResultList();
	}
	

}