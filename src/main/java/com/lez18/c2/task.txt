/*
	 * Creare un sistema di gestione Studente
	 * Uno studente è caratterizzato da:
	 * - Nome
	 * - Cognome
	 * - Matricola
	 * - DataNascita !!!
	 * 
	 * Creare (nello stesso Package) un sistema di gestione Esami
	 * Un esame è caratterizzato da:
	 * - Nome Esame
	 * - Codice Esami
	 * - Crediti
	 * - DataEsame !!!
	 * 
	 * Creare un web service che permetta
	 * 1) Operazioni di CRUD (Tutte tramite POSTMAN)
	 * 2) Ricerca per nome, nome e cognome, dati frammentari (di nome e cognome) - Andate a vedere su Restrictions!!!
	 * 3) Ricerca per nome Esame e per Data Esame
	 * 
	 * HARD:
	 * 1h) Creare un web service che mi permetta l'iscrizione di un utente ad un esame
	 * - ESEMPIO: Invio tramite post all'indirizzo http://localhost:<porta>/iscrivi/<stud matricola>/<codice esame>
	 * 
	 * 2h) Visualizzare l'elenco degli esami a cui è iscritto uno studente tramite indirizzo
	 * http://localhost:<porta>/listaesami/<stud matricola>
	 * 
	 * 3h) Visualizzare l'elenco degli studenti iscritti ad un esame
	 * http://localhost:<porta>/listaiscritti/<codice esame>
	 */