CREATE DATABASE studenti_spring;
USE studenti_spring;

CREATE TABLE studente(
	studenteID 	 INTEGER		NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome		 VARCHAR(250) 	NOT NULL,
    cognome		 VARCHAR(250) 	NOT NULL,
    matricola	 VARCHAR(10)  	NOT NULL UNIQUE,
    data_nascita DATE
);

CREATE TABLE esame(
	esameID 		INTEGER 		NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome_esame		VARCHAR(250) 	NOT NULL,
    codice_esame	VARCHAR(20)	 	NOT NULL UNIQUE,
    crediti_esame	INTEGER DEFAULT(1),
    data_esame		DATE
);

-- Tabella di appoggio che mi unisce studente ed esame (iscrizione)
CREATE TABLE studente_esame(
	studenteRIF INTEGER NOT NULL,
    esameRIF 	INTEGER NOT NULL,
    FOREIGN KEY (studenteRIF) REFERENCES studente(studenteID) ON DELETE CASCADE,
    FOREIGN KEY (esameRIF) REFERENCES esame(esameID) ON DELETE CASCADE,
    UNIQUE(studenteRIF, esameRIF)
);