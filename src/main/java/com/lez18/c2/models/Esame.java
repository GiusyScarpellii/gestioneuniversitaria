package com.lez18.c2.models;


import java.util.*;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "studenti"})
@Table(name="esame")
public class Esame {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="esameID")
	private int id;
	
	@Column
	private String nome_esame;
	@Column
	private String codice_esame;
	@Column
	private int crediti_esame;
	
	@Column
	private String data_esame;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "studente_esame",
			joinColumns = { @JoinColumn(name="esameRIF", referencedColumnName = "esameID") },
			inverseJoinColumns = { @JoinColumn(name="studenteRIF", referencedColumnName = "studenteID") })
	private List<Studente> studenti;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome_esame() {
		return nome_esame;
	}
	public void setNome_esame(String nome_esame) {
		this.nome_esame = nome_esame;
	}
	public String getCodice_esame() {
		return codice_esame;
	}
	public void setCodice_esame(String codice_esame) {
		this.codice_esame = codice_esame;
	}
	public int getCrediti_esame() {
		return crediti_esame;
	}
	public void setCrediti_esame(int crediti_esame) {
		this.crediti_esame = crediti_esame;
	}
	public String getData_esame() {
		return data_esame;
	}
	public void setData_esame(String data_esame) {
		this.data_esame = data_esame;
	}
	public List<Studente> getStudenti() {
		return studenti;
	}
	public void setStudenti(List<Studente> studenti) {
		this.studenti = studenti;
	}
	

	
	
	
}
