package com.lez18.c2.controllers;


import java.util.*;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lez18.c2.models.Esame;
import com.lez18.c2.models.Studente;
import com.lez18.c2.services.EsameService;
import com.lez18.c2.services.StudenteService;



@RestController
@RequestMapping("/esame")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class EsameController{
	
	@Autowired
	private EsameService serviceEs;
	@Autowired
	private StudenteService serviceStu;
	
	@PostMapping("/insert")
	public Esame inserisci_esame(@RequestBody Esame objEs) {
		return serviceEs.insert(objEs);
		
	}
	
	@GetMapping("/cercaTutti")
	public List<Esame> lista_esami(){
		return serviceEs.findAll();
	}
	
	@GetMapping("/cercaPerId/{esame_id}")
	public Esame cercaPerId(@PathVariable int esame_id) {
		return serviceEs.findById(esame_id);
	}
	
	@DeleteMapping("/cancella/{esame_id}")
	public boolean eliminaEsame(@PathVariable int esame_id) {
		return serviceEs.delete(esame_id);
	}
	
	@PutMapping("/modifica/update")
	public boolean modificaEsame(@RequestBody Esame varEs) {
		return serviceEs.update(varEs);
	}
	
	/*----------------------------------------------------------------------------------------*/

	/*
	 * Metodo per iscrivere uno studente ad un esame 
	 * PS: L'inserimento nella tabella d'appoggio studente_esame funziona ;)
	 */		
	
	@GetMapping("/iscrivi/{studente_id}/{esame_id}")
	public boolean iscrivi_studente(@PathVariable int studente_id, @PathVariable int esame_id) {
		
		Studente temp = serviceStu.findById(studente_id);
		List<Esame> esami_con_iscritti = new ArrayList<Esame>();
		
		
		Esame tempp = serviceEs.findById(esame_id);
		List<Studente> studenti_iscritti = new ArrayList<Studente>();
		studenti_iscritti.add(temp);
		esami_con_iscritti.add(tempp);
		tempp.getStudenti().addAll(studenti_iscritti);
		
		return serviceEs.update(tempp);
	}
	
	@GetMapping("/cercaPerNome/{nome_esame}")
	public List<Esame> cercaPerNome(String nome_esame) {
		return serviceEs.findByName(nome_esame);
	}
	
	@GetMapping("/cercaPerData/{data_esame}")
	public List<Esame> cercaPerData(String data_esame) {
		return serviceEs.findByName(data_esame);
	}

	
	
}
