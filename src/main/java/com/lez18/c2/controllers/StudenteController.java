package com.lez18.c2.controllers;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lez18.c2.models.Studente;
import com.lez18.c2.services.StudenteService;



@RestController
@RequestMapping("/studente")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class StudenteController {
	
	@Autowired
	private StudenteService service;

	@PostMapping("/inserisci")
	public Studente inserisci_studente(@RequestBody Studente objStud) {
		return service.insert(objStud);
	}
	
	@GetMapping("/cercaTutti")
	public List<Studente> lista_studenti(){
		return service.findAll();
	}
	
	@GetMapping("/cercaPerId/{studente_id}")
	public Studente cercaPerId(@PathVariable int studente_id) {
		return service.findById(studente_id);
	}
	
	@DeleteMapping("/cancella/{studente_id}")
	public boolean eliminaStudente(@PathVariable int studente_id) {
		return service.delete(studente_id);
	}
	
	@PutMapping("/modifica/update")
	public boolean modificaStudente(@RequestBody Studente varStud) {
		return service.update(varStud);
	}
	
	@GetMapping("/cercaPerNome/{nome_studente}")
	public List<Studente> cercaPerNome (@PathVariable String nome_studente) {
		return service.findByName(nome_studente);
	}
	
	@GetMapping("/cercaPerNomeECognome/{nome_studente}/{cognome_studente}")
	public List<Studente> cercaPerNomeECognome (@PathVariable String nome_studente, @PathVariable String cognome_studente) {
		return service.findByNameAndSurname(nome_studente, cognome_studente);
	}
	

}