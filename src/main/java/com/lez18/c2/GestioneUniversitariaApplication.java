package com.lez18.c2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestioneUniversitariaApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestioneUniversitariaApplication.class, args);
	}

}
